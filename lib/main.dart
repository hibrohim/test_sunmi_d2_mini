import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:sunmi_printer/sunmi_printer.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? _printerStatus = '';

  @override
  void initState() {
    super.initState();

    _bindingPrinter().then((binded) async => {
          if (binded!)
            {
              _getPrinterStatus(),
              _getPrinterMode(),
            }
        });
  }

  bool? bindingPrinterStatus;

  Future<bool?> _bindingPrinter() async {
    final bool? result = await SunmiPrinter.bindingPrinter();
    bindingPrinterStatus = result;
    setState(() {});
    return result;
  }

  Future<void> _getPrinterStatus() async {
    // Possible printer status
    //  static Map _printerStatus = {
    //   'ERROR': 'Something went wrong.',
    //   'NORMAL': 'Works normally',
    //   'ABNORMAL_COMMUNICATION': 'Abnormal communication',
    //   'OUT_OF_PAPER': 'Out of paper',
    //   'PREPARING': 'Preparing printer',
    //   'OVERHEATED': 'Overheated',
    //   'OPEN_THE_LID': 'Open the lid',
    //   'PAPER_CUTTER_ABNORMAL': 'The paper cutter is abnormal',
    //   'PAPER_CUTTER_RECOVERED': 'The paper cutter has been recovered',
    //   'NO_BLACK_MARK': 'No black mark had been detected',
    //   'NO_PRINTER_DETECTED': 'No printer had been detected',
    //   'FAILED_TO_UPGRADE_FIRMWARE': 'Failed to upgrade firmware',
    //   'EXCEPTION': 'Unknown Error code',
    // };
    final String? result = await SunmiPrinter.getPrinterStatus();
    setState(() {
      _printerStatus = result;
    });
  }

  Future<void> _getPrinterMode() async {
    // printer mode = [  NORMAL_MODE , BLACK_LABEL_MODE, LABEL_MODE ]
    final String? result = await SunmiPrinter.getPrinterMode();
    print('printer mode: $result');
    printerMode = result ?? 'Null printerMode';
    setState(() {});
  }

  // Only support Sunmi V2 Pro Label Version. P/s: V2 Pro Standard not supported for label printing
  Future<void> _printLabel(BuildContext context) async {
    try {
      // start point of label print
      await SunmiPrinter.startLabelPrint();
      // alignment , 0 = align left, 1 =  center, 2 = align right.
      await SunmiPrinter.setAlignment(0);
      await SunmiPrinter.lineWrap(1);
      await SunmiPrinter.printText('label');
      await SunmiPrinter.lineWrap(6); // must have for label printing before exitLabelPrint()
      await SunmiPrinter.exitLabelPrint();
      // end point of label print
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
    if (_printerStatus == 'Works normally') {
      print('printing..!');
    }
  }

  Future<void> _printTransaction(BuildContext context, String text) async {
    try {
      await SunmiPrinter.startTransactionPrint();
      await SunmiPrinter.setAlignment(0);
      await SunmiPrinter.printText(text);
      await SunmiPrinter.lineWrap(1);
      await SunmiPrinter.printColumn('Data', 'Sesuatu');
      await SunmiPrinter.lineWrap(6);
      await SunmiPrinter.submitTransactionPrint();
      await SunmiPrinter.exitTransactionPrint();
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
    if (_printerStatus == 'Works normally') {
      print('printing..!');
    }
  }

  Future<void> _printTransactionColumn(BuildContext context, String text) async {
    try {
      await SunmiPrinter.startTransactionPrint();
      await SunmiPrinter.setAlignment(0);
      await SunmiPrinter.printColumn('Data', 'Sesuatu');
      await SunmiPrinter.lineWrap(6);
      await SunmiPrinter.submitTransactionPrint();
      await SunmiPrinter.exitTransactionPrint();
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
    if (_printerStatus == 'Works normally') {
      print('printing..!');
    }
  }

  Future<void> _printTransactionColumnList(BuildContext context, String text) async {
    try {
      await SunmiPrinter.startTransactionPrint();
      await SunmiPrinter.setAlignment(0);
      await SunmiPrinter.printColumnList(
        ['Data', ':', 'Keterangan yang sangat panjang sehingga jadi 2 baris'],
        [12,1,12],
        [0,1,0],
      );
      await SunmiPrinter.lineWrap(6);
      await SunmiPrinter.submitTransactionPrint();
      await SunmiPrinter.exitTransactionPrint();
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
    if (_printerStatus == 'Works normally') {
      print('printing..!');
    }
  }

  String printerMode = '';
  String textPrint =
      'Testing Printer Sunmi D2 Mini dengan package sunmi_printer v1.0.3';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(textPrint, style: Theme.of(context).textTheme.headline4),
            const SizedBox(height: 16),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ElevatedButton(
                  child: Text(
                    'Print Transaction',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  onPressed: () {
                    try {
                      _bindingPrinter().then((binded) async => {
                            if (binded!)
                              {await _printTransaction(context, textPrint)}
                          });
                    } catch (e) {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(content: Text(e.toString())));
                    }
                  },
                ),
                const SizedBox(width: 16),
                ElevatedButton(
                  child: Text(
                    'Print Label',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  onPressed: () {
                    try {
                      _bindingPrinter().then((binded) async => {
                            if (binded!) {await _printLabel(context)}
                          });
                    } catch (e) {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(content: Text(e.toString())));
                    }
                  },
                ),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ElevatedButton(
                  child: Text(
                    'Cek Status Printer & Mode',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  onPressed: () async {
                    await _getPrinterStatus();
                    await _getPrinterMode();
                    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    //     content: Text(
                    //         '_printerStatus: ${_printerStatus ?? 'Null status'}')));
                  },
                ),
                // const SizedBox(width: 16),
                // ElevatedButton(
                //   child: Text(
                //     'Printer Mode',
                //     style: Theme.of(context).textTheme.headline4,
                //   ),
                //   onPressed: () {
                //     ScaffoldMessenger.of(context).showSnackBar(
                //         SnackBar(content: Text('printerMode: $printerMode')));
                //   },
                // ),
              ],
            ),
            const SizedBox(height: 16),
            Text('bindingPrinterStatus: ' + bindingPrinterStatus.toString(),
                style: Theme.of(context).textTheme.headline4),
            const SizedBox(height: 16),
            Text('printerStatus: ' + _printerStatus.toString(),
                style: Theme.of(context).textTheme.headline4),
            const SizedBox(height: 16),
            Text('printerMode: ' + printerMode.toString(),
                style: Theme.of(context).textTheme.headline4),
            const SizedBox(height: 16),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ElevatedButton(
                  child: Text(
                    'Print Column',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  onPressed: () {
                    try {
                      _bindingPrinter().then((binded) async => {
                        if (binded!)
                          {await _printTransactionColumn(context, textPrint)}
                      });
                    } catch (e) {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(content: Text(e.toString())));
                    }
                  },
                ),
                const SizedBox(width: 16),
                ElevatedButton(
                  child: Text(
                    'Print Column List',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  onPressed: () {
                    try {
                      _bindingPrinter().then((binded) async => {
                        if (binded!)
                          {await _printTransactionColumnList(context, textPrint)}
                      });
                    } catch (e) {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(content: Text(e.toString())));
                    }
                  },
                ),
              ],
            ),
            const SizedBox(height: 16),
          ],
        ),
      ),
    );
  }
}
